
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION 1.x
----------------
Themify Icons (https://themify.me) is a complete set of icons for use in web
design and apps.

“themify” provides integration of “Themify Icons” with Drupal. Once enabled
“Themify Icons“ icon fonts could be used as:

1. Directly inside of any HTML (node/block/view/panel). Inside HTML you can
   place Themify Icons icons just about anywhere with an <i> tag.

   Example for an info icon: <i class=“ti-wand”></i>

   See more examples of using “Themify Icons” within HTML at:
   https://themify.me/themify-icons

2. Icon API (https://drupal.org/project/icon) integration:
   This module provides easy to use interfaces that quickly allow you to inject
   icons in various aspects of your Drupal site: blocks, menus, fields, filters.


INSTALLATION
------------

1. Using Drush (https://github.com/drush-ops/drush#readme)

    $ drush pm-enable themify

    Upon enabling, this will also attempt to download and install the library
    in `sites/all/libraries/themify-icons`. If, for whatever reason, this process
    fails, you can re-run the library install manually by first clearing Drush
    caches:

    $ drush cc drush

    and then using another drush command:-

    $ drush ti-download

2. Manually

    a. Install the “Themify Icons“ library following one of these 2 options:
       - run "drush ti-download” (recommended, it will download the right
         package and extract it at the right place for you.)
       - manual install: Download & extract “Themify Icons“ 
         (https://themify.me/themify-icons) and place inside 
         "sites/all/libraries/themify-icons” directory. The CSS file should
         be sites/all/libraries/themify-icons/css/themify-icons.css
         Direct link for downloading latest version is: 
         https://github.com/lykmapipo/themify-icons/archive/master.zip
    b. Enable the module at Administer >> Site building >> Modules.
