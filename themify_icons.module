<?php
/**
 * @file
 * themify.module
 * Drupal integration with Themify Icons.
 */

define('THEMIFY_LIBRARY', 'themify-icons');
define('THEMIFY_NAME', 'Themify Icons');
define('THEMIFY_URL', 'https://themify.me');
define('THEMIFY_DOWNLOAD_URL', 'https://github.com/lykmapipo/themify-icons/archive/master.zip');
define('THEMIFY_PREFIX', 'ti');

/**
 * Implements hook_help().
 */
function themify_icons_help($path, $arg) {
  switch ($path) {
    case 'admin/help#themify':
      return '<p><i class="icon-flag icon-large"></i>' . t('<a href="!themify_url">@themify</a> is a complete set of icons for use in web design and apps.', array(
        '@themify' => THEMIFY_NAME,
        '!themify_url' => THEMIFY_URL,
      )) . '</p>';
  }
}

/**
 * Implements hook_libraries_info().
 */
function themify_icons_libraries_info() {
  $libraries[THEMIFY_LIBRARY] = array(
    'name' => THEMIFY_NAME,
    'vendor url' => THEMIFY_URL,
    'download url' => THEMIFY_DOWNLOAD_URL,
    'version callback' => 'themify_icons_version_callback',
    'files' => array(
      'css' => array(
        'css/themify-icons.css',
      ),
    ),
  );
  return $libraries;
}

function themify_icons_version_callback() {
  // The themify-icons.css file does not contain any version information, so
  // just reurn TRUE.
  return TRUE;
}

/**
 * Implements hook_preprocess_html().
 *
 * Purposefully only load on page requests and not hook_init(). This is
 * required so it does not increase the bootstrap time of Drupal when it isn't
 * necessary.
 */
function themify_icons_preprocess_html() {
  $library = libraries_load(THEMIFY_LIBRARY);
  if (!$library['loaded']) {
    drupal_set_message($library['error message'] . t('Please make sure that '
      . 'themify was download & extracted at sites/all/libraries/themify-icons directory. '
      . 'Please check README.txt for more details.'), 
      'warning');
  }
}

/**
 * Implements hook_icon_providers().
 */
function themify_icon_providers() {
  $providers[THEMIFY_LIBRARY] = array(
    'title' => THEMIFY_NAME,
    'url' => THEMIFY_URL,
  );
  return $providers;
}

/**
 * Implements hook_icon_bundle_configure().
 */
function themify_icons_icon_bundle_configure(&$settings, &$form_state, &$complete_form) {
  $bundle = $form_state['bundle'];
  if ($bundle['provider'] === THEMIFY_LIBRARY) {
    $settings['tag'] = array(
      '#type' => 'select',
      '#title' => t('HTML Markup'),
      '#description' => t('Choose the HTML markup tag that @themify icons should be created with. Typically, this is a %tag tag, however it can be changed to suite the theme requirements.', array(
        '@themify' => THEMIFY_NAME,
        '%tag' => '<' . $bundle['settings']['tag'] . '>',
      )),
      '#options' => drupal_map_assoc(array('i', 'span', 'div')),
      '#default_value' => $bundle['settings']['tag'],
    );
  }
}

/**
 * Implements hook_preprocess_icon_RENDER_HOOK().
 */
function themify_icons_preprocess_icon_sprite(&$variables) {
  $bundle = $variables['bundle'];
  if ($bundle['provider'] === THEMIFY_LIBRARY) {
    // Remove the default "icon" class.
    $key = array_search('icon', $variables['attributes']['class']);
    if ($key !== FALSE) {
      unset($variables['attributes']['class'][$key]);
    }
  }
}

/**
 * Implements hook_icon_bundles().
 */
function themify_icons_icon_bundles() {
  $bundles[THEMIFY_LIBRARY] = array(
    'title' => THEMIFY_NAME,
    'provider' => THEMIFY_LIBRARY,
    'render' => 'sprite',
    'settings' => array(
      'tag' => 'i',
    ),
    'icons' => themify_icons_extract_icons(),
  );
  return $bundles;
}

/**
 * Extracts all icons from the CSS file.
 *
 * @return array
 */
function themify_icons_extract_icons() {
  $library = libraries_load(THEMIFY_LIBRARY);
  $filepath = DRUPAL_ROOT . '/' . $library['library path'] . '/css/themify-icons.css';

  $content = file_exists($filepath) ? file_get_contents($filepath) : '';

  // Parse the CSS content
  if (preg_match_all('@\.ti-(.*?):before@m', $content, $matches)) {
    $icons = $matches[1];
    asort($icons);
    $keys = preg_filter('/^/', THEMIFY_PREFIX . '-', $icons);

    return array_combine($keys, $icons);
  }

  return array();
}
